Format: 1.0
Source: nebula-fail2ban-config
Binary: nebula-fail2ban-config
Architecture: all
Version: 0.1
Maintainer: Ernesto Nadir Crespo Avila <ecrespo@gmail.com>
Standards-Version: 3.9.2
Build-Depends: cdbs (>= 0.4.23-1.1), debhelper (>= 4.2.0), config-package-dev (>= 4.5~)
Package-List:
 nebula-fail2ban-config deb config extra arch=all
Checksums-Sha1:
 a759cad3412dc5574551162f562e6ab7470a41f5 6719 nebula-fail2ban-config_0.1.tar.gz
Checksums-Sha256:
 f48ec50e700de13be358fdaa4de856c005a7776f8a20326ed9b8b1df305c1d9a 6719 nebula-fail2ban-config_0.1.tar.gz
Files:
 31efa1da0f3e08df896c47f9037464f4 6719 nebula-fail2ban-config_0.1.tar.gz
